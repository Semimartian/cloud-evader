﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityManager : MonoBehaviour
{
    public static EntityManager Instance;

    public Cloud CloudPrefab;
    public Ad AdPrefab;
    public float NormalCloudSpeed;
    //public float slowMultiplier = 0.5f;
    public bool IsInsideCloud;
    private Cloud LastCloud;

    private static float MINIMUM_Y_SIZE = 1f;
    private static float MAXIMUM_Y_SIZE = 5f;
    private static float MINIMUM_X_SIZE = 1f;
    private static float MAXIMUM_X_SIZE = 5f;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    void Start()
    {
        /*StartCoroutine(SpawnClouds());
        StartCoroutine(SpawnAds());*/
        SpawnClouds();
        SpawnAd();
    }

    private void SpawnClouds()
    {
       // while (true)
        {
            LastCloud = Instantiate(CloudPrefab,transform);
            LastCloud.transform.Translate(new Vector3(Random.Range(-1.5f, 1.5f), -5f));
            float ySize = Random.Range(MINIMUM_Y_SIZE, MAXIMUM_Y_SIZE);
            float xSize = Random.Range(MINIMUM_X_SIZE, MAXIMUM_X_SIZE);
            LastCloud.transform.localScale = new Vector3(xSize, ySize, 0);
            //Instantiate(CloudPrefab, transform).transform.Translate(new Vector3(Random.Range(-1.5f, 1.5f), -5f));
            //yield return new WaitForSeconds(3);
            //SpawnAd();
            Invoke("SpawnClouds", 3);

        }
    }
    private void SpawnAd()
    {
        float X;
        if (LastCloud != null)
        {
            float CloudX = LastCloud.transform.localScale.x / 2f;
            //Debug.Log("CloudX=" + CloudX);
            for (int i = 0; i < 10; i++)
            {
                X = Random.Range(-1.7f, 1.7f);
                //This doesn'y work all the time for some reason
                if (X > LastCloud.transform.position.x + CloudX || X < LastCloud.transform.position.x - CloudX)
                {
                    Instantiate(AdPrefab, transform).transform.Translate(new Vector3(X, -5f));
                    break;
                }
            }
        }
        else
        {
            X = Random.Range(-1.5f, 1.5f);  
            Instantiate(AdPrefab, transform).transform.Translate(new Vector3(X, -5f));
        }

        Invoke("SpawnAd", Random.Range(5f, 20f));
    }
}
