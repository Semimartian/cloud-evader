﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cloud : MonoBehaviour
{
    // private CloudManager cloudManager;

   /* private static float MINIMUM_Y_SIZE = 1f;
    private static float MAXIMUM_Y_SIZE = 5f;
    private static float MINIMUM_X_SIZE = 1f;
    private static float MAXIMUM_X_SIZE = 5f;*/
   /* private void Start()
    {
        float ySize = Random.Range(MINIMUM_Y_SIZE, MAXIMUM_Y_SIZE);
        float xSize = Random.Range(MINIMUM_X_SIZE, MAXIMUM_X_SIZE);
        transform.localScale = new Vector3(xSize, ySize, 0);
        //cloudManager = CloudManager.Instance;    
    }*/

    void Update()
    {
        if (transform.position.y >= Absolutes.OUT_OF_SCREEN)
        {
            Destroy(gameObject);
            return;
        }

        if (Player.Instance.IsFallingFast)
        {
            transform.Translate(new Vector3(0, Time.deltaTime * EntityManager.Instance.NormalCloudSpeed * 2f));// * (cloudManager.isInSlowMode ? cloudManager.slowMultiplier : 1)));
        }
        else
        {
            transform.Translate(new Vector3(0, Time.deltaTime * EntityManager.Instance.NormalCloudSpeed));// * (cloudManager.isInSlowMode ? cloudManager.slowMultiplier : 1)));
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<Player>() != null)
        {
            EntityManager.Instance.IsInsideCloud = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.GetComponent<Player>() != null)
        {
            EntityManager.Instance.IsInsideCloud = false;
        }
    }
}
