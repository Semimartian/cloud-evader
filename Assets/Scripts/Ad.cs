﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ad : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<Player>() != null)
        {
            Destroy(this.gameObject);
            //Play Ad
        }
    }
    void Start()
    {

    }

    void Update()
    {
        if (transform.position.y >= Absolutes.OUT_OF_SCREEN)
        {
            Destroy(gameObject);
            return;
        }

        if (Player.Instance.IsFallingFast)
        {
            transform.Translate(new Vector3(0, Time.deltaTime * EntityManager.Instance.NormalCloudSpeed * 2f));// * (cloudManager.isInSlowMode ? cloudManager.slowMultiplier : 1)));
        }
        else
        {
            transform.Translate(new Vector3(0, Time.deltaTime * EntityManager.Instance.NormalCloudSpeed));// * (cloudManager.isInSlowMode ? cloudManager.slowMultiplier : 1)));
        }
    }
}
