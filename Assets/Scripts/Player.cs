﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    private static float SCREEN_EDGE = 2.8f;
    private static int STANDARD_COMBO_TIME = 10;

    public float XSpeed;
    public bool IsFallingFast = false;
    public static Player Instance;
    public int ScoreMultipier = 0;
    public Text ScoreText;
    public Text ScoreMultiplierText;
    public Text ComboTimeText;

    private int Score=0;
    private float scoreInterval = 0.5f;
    private bool TimeSkipper = false;
    private int comboTime;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    private void Start()
    {
        comboTime = STANDARD_COMBO_TIME;
        Invoke("UpdateScore", scoreInterval);
    }
    void Update()
    {
        IsFallingFast = Input.GetKey(KeyCode.DownArrow) && !EntityManager.Instance.IsInsideCloud;
        if (EntityManager.Instance.IsInsideCloud)
        {
            comboTime = STANDARD_COMBO_TIME;
            ScoreMultipier = 0;
        }
        if (!IsFallingFast)
        {
            //transform.Translate(new Vector3(Input.GetAxis("Horizontal") * Time.deltaTime * XSpeed, 0));
            if (Input.GetKey(KeyCode.RightArrow))
            {
                transform.Translate(new Vector3(Time.deltaTime * XSpeed, 0));
            }
            else if (Input.GetKey(KeyCode.LeftArrow))
            {
                transform.Translate(new Vector3(Time.deltaTime * -XSpeed, 0));
            }
        }
        

        //What happens when the player is at a screen border:
        if (transform.position.x > SCREEN_EDGE)
        {
        transform.position =new Vector3(-SCREEN_EDGE, transform.position.y,transform.position.z);
        }
        else if (transform.position.x < -SCREEN_EDGE)
        {
        transform.position = new Vector3(SCREEN_EDGE, transform.position.y, transform.position.z);
        }
    }

    private void UpdateScore()
    {
        Invoke("UpdateScore", scoreInterval);
        if (!EntityManager.Instance.IsInsideCloud)
        {
            if (TimeSkipper || IsFallingFast)
            {
                Score += ScoreMultipier;
                ScoreText.text = "" + Score;
                ScoreMultiplierText.text = "" + ScoreMultipier;
                ComboTimeText.text = "" + comboTime;
                if (TimeSkipper)
                {
                    comboTime--;
                    if (comboTime < 0)
                    {
                        ScoreMultipier++;
                        comboTime = STANDARD_COMBO_TIME;
                    }
                }
            }
            TimeSkipper = !TimeSkipper;
        }
    }
}
